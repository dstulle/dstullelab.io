---
title: ABOUT
---

Born in [Berlin󠁤󠁥󠁢󠁥󠁿](https://www.openstreetmap.org/relation/62422), studied computer science in Berlin and [上海](https://www.openstreetmap.org/relation/913067). Husband, father, boyscout and passionate game developer.
