---
title: CVT - Chinese Vocable Trainer
date: "2007-01-01"
jobDate: 2007
work: [programming]
techs: [ruby, libglade]
thumbnail: cvt/gkrellShoot_05-29-07_210800.jpeg
---

A simple Application to learn chinese translations and characters. I was not able to find an application to learn the characters, pronounciation and translation at the time so I just wrote my own tool learn these things.

![](gkrellShoot_05-29-07_210831.jpeg)
![](gkrellShoot_05-29-07_210932.jpeg)
