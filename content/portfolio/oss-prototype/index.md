---
title: Open Space Shooter - Prototype
date: "2006-01-01"
jobDate: 2006
work: [gamedevelopemnt]
techs: [C++, libSDL]
designs: [gimp]
thumbnail: oss-prototype/gkrellShoot_06-23-06_181405.jpg
projectUrl: oss-prototype-win.zip
---

Open Space Shooter was my first bigger game project using an actual game library, [SDL](https://www.libsdl.org/). The Idea was to have a simple top-down space shooter similar to asteroids. To make it more interesting I also added homing missiles, enemy ships, power ups and a leveling system.

![](gkrellShoot_06-23-06_180229.jpg)
![](gkrellShoot_06-23-06_180638.jpg)
![](gkrellShoot_06-23-06_180707.jpg)
![](gkrellShoot_06-23-06_180833.jpg)
![](gkrellShoot_06-23-06_181309.jpg)
![](gkrellShoot_06-23-06_181405.jpg)
![](gkrellShoot_06-23-06_182347.jpg)
![](gkrellShoot_06-23-06_182508.jpg)
![](gkrellShoot_06-23-06_182628.jpg)
![](gkrellShoot_06-23-06_182835.jpg)
![](gkrellShoot_06-23-06_182955.jpg)
