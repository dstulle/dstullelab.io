---
title: Open Space Shooter - Ground Troop Command
date: "2020-01-01"
jobDate: since 2018
work: [gamedevelopemnt]
techs: [kotlin, libGDX]
designs: [aseprite, pyxel]
thumbnail: oss-gtc/city-map.png
projectUrl: https://gitlab.com/microwood/open-space-shooter/ground-troop-command
---

Ground Troop Command as part of a greater Space Shoooter series will focus only on one division of the military complex:
The Ground Troop division with its infantry and vehicles to conduct military operations on a planet or in a ship.

The Story is set in the far future.
After the collapse of a human space empire several cultures emerged and formed a new alliance, the United Republic of Planets (URP).
The player takes control of a soldier in the armed forces of the URP, the United Defense Forces (UDF).

![](city-map.png)
![](sniper-camera.png)
![](rocky-map.png)
![](forest-map.png)
![](character-details.png)
![](mission-selection.png)
